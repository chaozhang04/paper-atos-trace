\section{Dynamic Path Evaluation}
\label{sec:dynamic}

In order to evaluate the results brought out by static path traversing, % {\em Static Path Traverse}
dynamic path executions are required to record traces and discover code self-modifications in time,
including the inconsistent forced execution of the {\em pre-path} and the trace of the following path.

\sysname first replays the execution on the {\em pre-path} with the static traversing results to avoid repeated analysis,
directly guiding the program to the target.
Inconsistent methods are performed during the process to overcome the limitations of symbolic execution.

When the program reaches the target,
the analysis system then begins the tracing process to find new code and new control flow transfers according to the current CFG.

If the trace is interrupted by exceptions with no registered handlers
or attacks to the analysis engine,
it is re-executed under unintentional event handling that avoids the interference and maintains sustainable execution.

\subsection{Inconsistent Forced Execution}
\label{sec:dynamic:inconsistent}

There are a number of situations that require special handling on inconsistent execution,
including the registers involved in the indirect control flow transfers,
the related input from outside devices
and the exceptions occurred during the execution without a registered handler, etc.


{\em Conditional Jump Targets} can be parsed directly by static analysis,
\sysname directly sets the value of {\em EIP} register to the address target
after the conditional jump instruction executed.
%The target address is obtained from static traversing output.
The control flow will be at the new branch immediately in this way.

{\em Indirect Transfers Targets} must be controlled by some source registers, 
the value of the registers are given in section~\ref{sec:cfg:indirect}, 
as well as the instructions that write the registers.
\sysname inconsistently set the value as the given one after the given instructions are executed.
The control flow will go to a new indirect target after the calculation of the given register.

{\em Exceptions} are executed inconsistently in three cases.
(1) If the an unregistered exception occurs, \sysname will recover the exception
inconsistently and reply the trace immediately, which will be demonstrated in \ref{sec:dynamic:sustainable};
(2) If a registered exception occurs, \sysname will recover the exception inconsistently
and record the way that can bypass the execution, which will be demonstrated in \ref{sec:cfg:exception};
(3) If there is a registed exception handler, but no exception occurs till all {\em pre-paths} have been dynamically evaluated,
\sysname will trigger the exception inconsistently.
It first looks for a memory operating instruction or a division instruction.
Then, 0 is filled in the memory address or the dividend register to trigger the exception filter function.
The code in the filter function will be explored in this way,
as well as the exception handler code.

{\em The calls to API that read data from devices} are {\em nopped}
and the output buffer of the API is written by random datas.

% Since most malware contend for analysis systems by hiding the actual executed code from static analyzers,
% the dynamic path evaluation process needs to traverse the binary at runtime.
% Therefore, multiple methods are proposed to ensure the inconsistent execution along the {\em pre-path},
% focusing on targets including registers, API return values, the memory and the stack.


% The paths are executed by indirect control flow transfer guiding methods given by the static path traversal.
% \sysname records the evaluated targets during the update process
% and terminates the current trace when guided to an explored target due to the inaccuracy of static analyses.
% Loop structures widely used in programs contain massive redundant instructions which causes serious overheads.
% The structures are identified dynamically through comparisons and the loop instructions using the static results are skipped.





\subsection{Instruction Level Monitor}
\label{sec:dynamic:trace}

As discussed above, the statically obtained CFG is not complete because of the various obfuscations in malware.
{\em New code/edges} are defined as the code or edges not obtained from the CFG,
and they can be found at any time during the execution,
both at the control flow transfer and sequential~\cite{Yadegari-symbolic} instructions.
Therefore, instruction-level monitors are essential for complete CFG extractions.

% to-do: can be reduced ....
The control flow transfer types of all instructions are recorded.
Specifically, the instructions inside a basic block are of sequential execution;
the last instruction of a basic block can be a conditional jump, an indirect jump or a {\em ret},
their types being jump, indirect jump and function return respectively;
call instructions are recorded as direct and indirect function calls.
All indirect jumps, indirect calls, function returns and instructions causing exceptions during the execution process
are regarded as indirect control flow transfer in the following sections.
%

\sysname also maintains a shadow CFG during the execution.
A pointer is initially pointed to the entry point of the {\em main} function
or the program entry point if the program starts abnormally,
the target of which moves on the shadow CFG according to the execution.
The detection is applied based on the comparison between
the pointed instruction in the shadow CFG and the real executed instruction.

\subsubsection{New Code/Edge Detection}
Since different instructions may overlap the same address due to self-modifications,
tuple {\em (address, hash)} is used for the identification of instructions, basic blocks and functions,
representing the start address and binary code hashes of the items.

The overall process first checks the addresses first for given instructions.
If the address is consistent between the shadow CFG record and the execution,
the hash of the instruction is also checked in case of self-modifications.
The execution is consistent with the obtained CFG if the address and the hash are the same.
If the address is not consistent, the hash is not checked.

Specifically, for sequential instructions,
inconsistent addresses indicate an unknown problem of the static analysis,
so the instruction is processed as {\em jmp}/{\em call} for further checks.
If the hashes are not the same,
code self-modifications have occurred and the whole function needs to be re-analyzed,
thus the instruction is treated as {\em jmp} for further checks.

For the {\em jmp}/{\em cJmp} instructions,
if the instruction jumps to the head of a library function,
it is processed as a {\em call}.
If it jumps to an existing block in the current function, directly add the edge.
If it jumps to a block of another function,
add the edge to the CFG as well as a mark for merging during CFG update in the CFG Management process.
If it jumps inside a function and the target is inside a block,
add an edge to the function and repeat the whole {\em jmp}/{\em cJmp} process routine.
If it jumps inside a function but the boundary is recognized incorrectly, i.e. the target is on the chunk,
a new function is created and the {\em jmp}/{\em cJmp} process is repeated.
If the jump target is not even in the function area,
the static analysis needs to be redone to check whether the {\em jmp} target is a standard function header.
If so, mark the control flow transfer as a {\em call};
otherwise, mark it for further merging in the CFG Management process.

For {\em call} instructions,
the instruction is treated as {\em jmp} if the address called is not a normal function or is within a known function.
If the call target is the start a known function but the static analysis failed to recognize the call, add the FCG edge.
A call to an unknown area will cause the static analysis at the target address and then a re-execution of the {\em call} process routine.

For the {\em ret} instructions,
if the return address is inconsistent with the statically recognized return address,
treat the instruction as {\em jmp} since no representation of the current call stack can be found.

The indexing method above can locate an instruction in {\em O(1)} time in most cases.
% which is a great progress compared to other indexing methods.  % TODO: can we say so?
For the basic blocks unable to locate through the CFG edge relationship in {\em O(1)},
\sysname uses a combination of the instruction address and byte code for indexing
and a two-level indexing (block pointer and instruction pointer) to ensure high efficiency.
Besides, details of library function calls are not tracked because the static analysis can identify most of them.

\subsection{Sustainable Execution}
\label{sec:dynamic:sustainable}

A guard thread and a time limit is set for dynamic path evaluation in case of situations
that may lead the execution to unexpected states (without exception handlers),
such as inconsistent execution, attack to analyzer, large or infinite loop and deliberately set exceptions.
Sustainable execution identifies these situations and re-execute the abnormal path by recovering the exceptions.


All the cases can be found by comparison between the trace and the CFG.
As shown in section~\ref{sec:dynamic:trace},
\sysname is able to locate the last instruction item in the CFG
and the next instruction can be predicted in a similar way.
If there is unexecuted code defined by other user after the last recorded instruction,
unexpected situations must have occurred.

\subsubsection{Loop Breaking}

If the program is still executing when the time limit is reached,
the program has fallen in a loop.
\sysname backward locates each instruction item in the CFG, looking for a loop structure.
It analyzes the CFG statically to identify the largest loop or recursion related to the last instruction,
finding the breaking points at the same time.

The loop structures are determined by backtracking all the instructions in the trace
and comparing them with the CFG pair to find the current loop.
The recursions are identified by backtracking all the function calls in the trace
and comparing them with the FCG to identify the recursive function calls.


During the next execution,
\sysname replays the same {\em pre-path} as the former execution,
running to any of the breaking points without detecting new code/edges.

If no breaking point is hit in the first half of the time limit,
\sysname forces the execution to one of the breaking points and enables the instruction-level monitor.


\subsubsection{Exception Recovery}
Backward slicing to the last instruction is performed on abnormal paths
to figure out what has caused the exception.
There are several cases.

First, \sysname checks whether the exception is caused
by an isolated data flow instruction or an API call without input.
If so, it indicates a probable attack to the analysis platform.
\sysname bypasses the related instruction with {\em nop} in the next execution.
The last instruction is also replaced by {\em nop} if it is an API call and the program stuck in it.
\sysname also tries to fill in the output buffer and the return value of the API call.

Then, \sysname analyzes all the registers used by the instruction that triggers the exception
and checks whether the values depend on the program inputs.
The independence indicates the possibility of avoiding the exception.
In such cases,
\sysname leaves the exception for further exploration and records the current analysis results
because the exception may be eliminated in other explorations.

If it is not sure whether the exception is related to program inputs,
\sysname recovers the exception with the~\cite{X-Force} method and inconsistently assigns the related values.
Take divide-zero-exception as an example.
\sysname identifies the instruction operands that caused the exception,
analyzes its addressing mode,
points the pointer to a readable and writable memory area
and reassigns the operable registers including {\em eax} and {\em ebx}
(registers like {\em esp} and {\em ebp} are not operable).
If it is a divide instruction and the divisor is not a memory value but a register or a register calculation,
the operable register is modified so that the result is not zero.

If the above method cannot recover the exception,
\sysname analyzes whether the instruction that triggers the exception has a data dependency with the path constraints.
It also reversely backtracks from the instruction using data flow analysis to see if the slice contains instructions related to the constraints.
If there is no relationship,
\sysname replaces the instruction fragment with {\em nop};
otherwise, the constraints are solved by the traditional way.

\subsubsection{Analysis Engine Protecting}

The analyzer may not notice some of the attacks to it.
Since the only consequence is the incomplete trace,
\sysname regards the situation as an exception.
The solutions above can deal with the case properly.




