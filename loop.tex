\section{Setting Check Points}
\label{sec:loop}

Reductant code is caused by calling to a function many times and
executing loop structures.

With the common tracing methods discussed in former sections,
\sysname is able to achieve the following goals:
(1) set the tracing granularity adjustably before calling into the function
(2) resist the self-modification technique.
(3) find new code or new edges in online mode


\subsection{Setting Check Intre-Procedure Points}

Initially, instruction check points are set on the same address as the pointer to the shadow CFG,
which is the entry point of the first user-defined function.
Instruction check points are set on each code of the function,
as the tracing state of the function is \textit{un-traced}.

In later tracing, when the current instruction traced is a call instruction,
\sysname sets new check points for the code in the target function.
Based on the types of functions,
we separately introduce the way to set check points of user-defined functions and library functions.

Note that since the methods to set check points according to the tracing granularity are common,
the details of setting check points for instruction level tracing and basic block level tracing are introduced in section \ref{sec:AAG:setcc:common}.

\subsubsection{Setting Check Points for User-Defined Function}

When the current instruction is a function call instruction whose call target is a user-defined function
with \texttt{un-traced} state, the function will be tracing under instruction level.

When the current instruction is a function call instruction whose call target is a user-defined function
with \texttt{traced} state,
\sysname calculates the hash of the current code of the function.
If the hash is not the same as the one recorded in the shadow CFG,
it indicates that new code generated in the function.
The function will be traced under instruction level and instruction check points are set at each instruction of the function.
Otherwise, the function will be traced under block level tracing.

When the current instruction is a function call instruction whose call target is a user-defined function
with \texttt{half-traced} state,
the function will be traced in a mixed mode.
The basic blocks with \texttt{traced} state will be traced under basic block level
and the \texttt{un-traced} ones will be traced under instruction level.
The check points are set according to the granularity.

Besides, W \& E check points are set for the basic blocks that are \texttt{traced}.
When the code is under coarse-grained tracing,
and there is modification to one of the code,
the W \& E check points will be hit,
which indicates that the function is modifying itself.
At this time, instruction check point is set on the modified address to monitor the modification.


\subsubsection{Setting Check Points for Library Function}


\sysname does not record the detail of library function call.
If the function call is a library function, an \texttt{instruction check point} is set on the next instruction of the caller instruction.
Also the function call is recorded in the shadow CFG.
If the function call target is unknown to the current shadow CFG,
a new edge is added.

Besides, W \& E check points are set on code of all the statically linked library functions,
in case that the program dynamically modified the contents of them.

\subsubsection{Setting Check Points for Indirect Control Flow Transfer}

When \sysname finds an unknown indirect jump target (indirect call has been dealt),
it first check whether the target is within the current function.
If not, \sysname merges the sub-CFG into the CFG of current function,
new check points are set on them as well.

\subsubsection{Common Methods for Setting Check Points}
\label{sec:AAG:setcc:common}

The way of setting check points according to tracing granularity is common.

For \textit{\textbf{instruction level tracing}}, instruction check points are set at each instruction in the function.

For \textit{\textbf{basic block level tracing}}, block check points are set on the last instruction of each basic block.
Note that if there is call or indirect jump or ret instructions in a basic block tracing under basic block level tracing,
instruction check points are set on them.

If the current traced instruction is a loop body and all instructions in the loop body have been traced,
instruction check points will set on the instructions that will be executed right after all the loop body code executed.
\sysname skips the reductant execution of simple loop structures.
The details of folding the loop structures, also called the loop level tracing,
will be introduced in section \ref{sec:loop}.


\subsection{Setting Check Intra-Procedure Points}

In this section, we will discuss how to fold the loop structures,
according to the initiation discussed in section \ref{sec:design:initiation},
to reduce the reductant traced code caused by executing loop structures within the function,
which is called loop level tracing.
Since some functions may have very complex internal logic and the cyclomatic complexity (CC for short) is very large,
we introduce different methods for different cases.
We also discuss the reducible method for complete trace in both of the two methods.
A brief comparison is discussed at last.

\subsubsection{Fold for Normal Functions}

Unlink other static and dynamic analysis combined solutions in which
static analysis is only performed before dynamic analysis begins,
\sysname performs static analysis whenever \sysname finds new code or edges.
At initial time, \sysname knows nothing about the program,
so it statically obtains the shadow CFG of the program.
Simple cycles (loop structures) and strongly connected components (SCC) are identified according to the CFG by
Johnson's algorithm~\cite{simple-cycle} and Kosaraju's algorithm~\cite{algorithms}.
The cycle or SCC consists of basic blocks.
For each cycle or SCC, XX check points are set on the basic blocks that satisfied Equation~\ref{equ:cp_for_loop},
which are the ones that one or more of their successors is not in the cycle or SCC.
YY check points are set on the successors that are not in the cycle and
the successors (marked as oSCC) that are not in the SCC are recorded for further usage.

\begin{equation}
\label{equ:cp_for_loop}
b\in C \wedge \exists s\left ( s\in succ\left ( b \right ) \wedge s \notin C \right )
\end{equation}
In which \textit{C} is the basic block sequence of the cycle or SCC,
\textit{succ}(\textit{b}) means the successors of basic block \textit{b} in the CFG.

The same operations are performed whenever there are new edges or new code found.

During the tracing, when XX check points are hit, \sysname checks
whether all code in the corresponding cycle has been traced successively (ignoring the function calls in them) before the check point.
If so, it indicates that the loop structure has been traced and loop level tracing can be enabled.
The later execution of the code in loop body is traced under basic block level or simply skipped according to the user's configuration
until YY check points are hit.

If the instruction (marked as ins) of YY check point and the cycle are in the same SCC,
ZZ check points are set on oSCC and on the first instruction of each \texttt{un-traced} basic block in the SCC.
The trace from ins to oSCC can be recorded under basic block level or simply skipped according to the user's configuration
until one of ZZ check point is hit.

%\subsubsection{Reducible Trace}

The recovery of basic block level tracing is the same as discussed in section \ref{sec:dynamic:bb-trace}
The skipped code of the cycles can be recovered by counting the loop number and filling with the loop body obtained by static analysis.
If the complete trace is required, Counter check points should be added to count the execution number of the loop body.
But the skipped code of the SCC can not be recovered.
User can configure this for the cases that do not care the detail of complex loop structures, e.g., the basic block coverage based applications.


\subsubsection{Fold for Large CC Functions}

When the cyclomatic complexity is large (40 based on our observation),
there will be large mount of circles available in a CFG.
At this time, the overhead for loop identification will be unacceptable.
So, we proposes a supplement for this, which is a loosen solution.
XX check points are set to adjust the granularity of the current function.

The key to this solution is where to set the check points.
When calling to the functions with large CC,
\sysname gives a unique magic number for the function.
The magic number is marked on each traced object.

When \sysname finds the currently traced instruction has already marked the magic number,
new check points are set to the function.
Only the \texttt{un-traced} code are with instruction check points.
The \texttt{traced} code are with block check points or simply no check points.

\subsubsection{Compare between the Two Fold Method}

Folding method for large CC functions is not able to give the loop structure information.
The basic block level tracing can reduce some code but can not really reduce the reductant code.
Folding method for small CC functions can recover the loop body with the help of static analysis.

Fortunately, the number of functions with very large CC functions is small.
And in most cases, the functions are in charge of dispatching messages or simply obfuscated.
The analyzer should trace all the code for both of these cases,
because data dependency relationship is always needed in analyzing such programs and coarse-grained solutions can not achieve it.
