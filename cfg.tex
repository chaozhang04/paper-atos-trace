\section{Loop Collapse}
\label{sec:cfg}

Dynamic path evaluation revises the static results.
Before selecting the next exploring target based on the CFG,
\sysname updates the CFG according to the dynamic trace,
including the exploration state of instructions, paths and each of the control flow transfers.

\subsection{Timely CFG Updating}

\sysname updates the CFG once receives the trace,
mainly upon two parts: the exploration state of the CFG and the CFG structure.
The exploration state can be updated according to the state transitions in~\ref{sec:static:transition}.
The update to the CFG structure consists of three phases: adding nodes, edges and CFGs.
\sysname examines the operations introduced in~\ref{sec:dynamic:trace}
and checks whether there are conflicts since multiple paths may find the same code region simultaneously.
{\em (addr, hash)} is used as the identification of different items.

As code may be released multiple times at runtime,
the CFG of the program is timely updated.
{\em Backward merging} and {\em Wrong Cases Eliminating} are also performed to record the CFG correctly.

{\em Adding New Nodes.}
Given a newly found basic block,
\sysname first checks whether there is already a node maintained with the same address and hash.
If no record is found, the node is added to the CFG.

{\em Adding New edges.}
For each newly detected edge {\em (a, b)},
\sysname looks for recorded nodes of {\em a} and {\em b} and checks whether the edge already exists.
If not, the edge is added to CFG.

{\em Adding New CFGs.}
For a normal new CFG, the nodes and edges are added according to the above operations.
For the CFGs that should be merged marked by the path evaluation,
\sysname merges the CFGs as well as eliminating the wrong ones.
Besides, the tuple identification solutions avoid the situation that
different self-modified code shares the same region.

\subsubsection{Backward Merging}
Since static analysis is vulnerable,
the statically obtained CFGs are mostly incomplete.
A complete CFG should be analyzed for several times.
Backward merging helps merging the incomplete CFGs into a complete CFG.

% TODO: what does 'backend merging' generally do? add such introducing sentence?
Given an address {\em ea}, build a new function {\em F} and the basic block {\em B} with {\em ea} being the start address.
Generate the CFG {\em G} by analyzing the control flows.
For another address {\em ea'} and its function {\em F'}, basic block {\em B'} and CFG {\em G'},
calculate the code areas {\em Q} and {\em Q'} (possibly non-connected) respectively for {\em G} and {\em G'}.
For each edge represented as {\em (node1, node2)} in {\em G}, add the edge to {\em G'}.
If neither {\em node1} or {\em node2} is in the address interval {\em Q'}, also add {\em (B', B)} to {\em G'}.
Otherwise, decide whether to remove the unreachable subgraphs in {\em G}
and do path explorations both for {\em G} and {\em G'} if not.
The problem of different instructions stored in the same address in two control flow graphs can be overcome by such CFG mergence,
thus the function bound problem of static analysis can be solved.

\subsubsection{Wrong Cases Eliminating}  % TODO: decide the name
For the functions {\em A} and {\em B} backward merged into function {\em C},
there is a control flow transfer {\em (a, b)} from {\em A} to {\em B}.
Suppose the original control flow transfer in function {\em A} is {\em (a, c)}.
First check whether {\em (a, c)} is feasible.
If instruction {\em a} uses the instruction overlap technique which is a way to implement code self-modification~\cite{CoDisasm-RN476},
there is no control flow transfer from {\em a} to {\em c} since the next instruction of {\em a} is located within {\em c}.
% TODO: is this 'as' for both or only the latter?
Directly replace function {\em A} with function {\em B}
and remove the subgraph of CFG following the unreachable instruction {\em c} as {\em B} is backward merged from {\em A}.
The backward data dependency analysis helps achieving this by reporting whether the control flow target of {\em a} is unique.

% TODO: decide the name
\subsection{Exploration State Recording}

% TODO: check the meaning
When an indirect transfer instruction or a handled exception is confirmed by the path evaluation for the first time,
% TODO: decide the 'CFG Managing' name
CFG Management tries to locate the source registers that control the indirect transfer targets or lead to the exception.
The values of the registers are also predicted for different targets.
If all the potential values of the registers have been explored,
the exploration status of the indirect transfer instruction or the exception is changed to {\em done}.

\subsubsection{Handling Exceptions}
\label{sec:cfg:exception}


For the exceptions that have been triggered and then dealt by registered handlers,
\sysname infers the source registers that lead to the exception.
The values of the registers are then predicted by exception modeling.
The handling process is the same as the one used in section~\ref{sec:dynamic:sustainable}.
Note that the exceptions mentioned here are the ones captured during the execution,
and then dealt by exception handlers,
but the exceptions dealt in section~\ref{sec:dynamic:sustainable} are the ones that
do not have registered exception handlers for them.

For the exceptions that are un-triggered but handlers are registed for them,
\sysname tries to analyze the filter function under SEH~\cite{seh}.
If the type of the exception handler can be retrieved,
% for example, the specific number of excetpion is found, --- need to confirm
\sysname iterates all the instructions that can be caught by the exception handler
to find the instruction that can trigger the exception.
If the later path evaluation do not trigger the exception,
CFG Manager will set a flag for path evaluation to trigger the exception inconsistently, which is discussed in \ref{sec:dynamic:inconsistent}.
The exception filter functions are explored in this way,
so are the exception handlers.

% to-do, this paragraph is better-written than the above paragraph, should be merged.

% If the exception may not occur, i.e. the problem inputs may not trigger the exception,
% the following instructions and exception handlers still need to be evaluated.
% \sysname checks whether all exception handling code is covered
% and looks for the instructions triggering the exceptions of the handler filter requiring type.
% Remain randomly picking an instruction from the satisfying set and forcing it to trigger an exception
% until there is no uncovered exception handling code or the number of picks is no less than half the size of the set.
% Randomly pick an instruction in the picked instructions and perform path evaluations on it,
% regarding the handling code as a function and exploring the uncovered part.
% Therefore, \sysname allows the program be freely guided to the exception handling code and reduces the exception recovery due to inconsistent executions,
% also preventing from the influences caused by the exceptions on the analyses intentionally set by the program author.

% \paragraph{instruction exploration state}

\subsubsection{Handling Indirect Transfers}
\label{sec:cfg:indirect}

Indirect control flow transfers caused by indirect {\em jmp}/{\em call} and {\em ret} instructions
calculate the subsequent instruction address at runtime.
The transfer targets are hard to be resolved by current work~\cite{ida}.
It is proved by~\cite{resistance-RN636,resistant-RN355} that if the indirect target is only related to the program input
or calculated by complex algorithms, e.g. one-way functions,
it is impossible to get to know the transfer targets in reversing methods.

Fortunately, the calculation result must be a valid program address regardless of what mathematical transformations are used.
In addition, since the purpose of the indirect transfers are mostly to evade the detection of analyzers,
certain fixed or measured values are tend to be used to judge the environment.

% to-do: where to place it
Note that since {\em ret} targets can be controlled by modifying the position or the value of the return address on the stack,
\sysname monitors all the instructions that writes the return value address of each function.

\paragraph{Indirect Transfer Target Record}
Backward data flow analysis is performed starting from the indirect transfer instruction.

% TODO: check the meaning
\sysname infers which instructions are influencing the final targets
by backward analyzing the data dependency of each instruction.
The source operands of an instruction are defined as its input and the destination operands as output,
considering both explicit and implicit operands.
\sysname records the values that may lead to different indirect transfer targets.
A standard backward data dependency analysis algorithm is used whose details are shown in Appendix~\ref{alg:dataflow}.
% TODO: check the expression
Some important details are highlighted here.

% TODO: removed the ordering numbers of paragraphs

The backward iteration stops if the instruction meets the following cases:
(1) the single input is an immediate value;
(2) the instruction is {\em xor} or {\em cmp} or other instructions that can be used for comparison;
(3) the input is the {\em flags} register or a bit of it;
(4) the input is read from devices, such as the command line, network messages, files or other hardware devices, etc.
(5) the input is the value operation of specific {\em teb}/{\em peb} values;
(6) the input is the return value of an API call.

There are several cases that \sysname uses to predict the indirect transfer targets.
When the source register that affects the instruction input is a single fixed value,
\sysname records all the possibilities of the value.
If the input is the return value of an API,
all the possible return values are queried from MSDN~\cite{fireeye-msdn} and recorded for the instruction.
If the input is a specific {\em teb}/{\em peb} value, all the possible values are recorded.
If the input is a bit of the {\em flags} register, 0 and 1 are recorded.

There may also be two inputs that influence the target.
If one of them is a fixed value {\em x}, e.g. a fixed operand of {\em xor} or {\em cmp} instruction,
and the other value is an unknown external value, e.g. obtained from files,
then the value of the unknown value is recorded as {\em x-1}, {\em x} and {\em x+1}.
If both inputs that control the target are unknown values,
suppose one of them is {\em x} and the other is {\em x}, {\em x-1}, {\em x+1}, 0xFFFFFFFE and 0 in the following executions.

If there are three or more inputs that influence the target and two or more of them are unknown values,
some random values are recorded.

The above records may lead to some wrong or redundant target values.
% TODO: check the meaning
\sysname utilizes the methods presented in Multiverse~\cite{multiverse-RN655} to avoid the wrong addresses.
Besides, the execution is terminated immediately if the target has been explored during the execution.

Take code snippet~\ref{code:motivation2} as an example.
{\em t1} and {\em t2} are considered as the sources of the indirect jump target ({\em eax} in L9).
Backward slicing stops at L2 and L3 because the target {\em eax} is influenced by {\em t1} and {\em t2},
% TODO: check the meaning
whose inputs are a pre-set immediate value and a file content value.
Suppose the pre-set value is {\em x},
% TODO: sequence different from the one above?
\sysname records {\em t2} as {\em x}, {\em x-1}, {\em x+1}, 0xFFFFFFFE and 0.
It is obvious that all the possible targets can be explored in later executions.

Note that utilizing dynamic taint analysis (DTA)~\cite{all-you-ever-wanted-to-know} and value set analysis (VSA)~\cite{vsa} for indirect targets resolving
has poor maneuverability because too many tainted values are propagating in the analyzer.

% to-do: where to palace this?????
For stack operations,
% TODO: 'with' be replaced by 'from'?
\sysname needs to analyze whether the operand directly calculated with {\em esp} or {\em ebp} is an immediate value.
If so, the stack register and the immediate value (combined as SN) need to be treated as one input.
% TODO: replaced 'the stack register  and the immediate value' with 'the SN'
The backward tracking then continues until the output of instruction {\em I'} contains the stack register or the SN.
