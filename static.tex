% \vspace{-0.3cm}
\section{Static Path Traversal}
\label{sec:static}

% \vspace{-0.3cm}
{\em Static Path Traversal} is responsible for selecting the target basic blocks.
An intuitive strategy is presented and the details are discussed in this section.
% An intuition-based strategy is presented under two core principles.
% This section will discuss the details of it.

%\vspace{-0.3cm}
\subsection{Principles and Intuition}
\label{sec:static:principles}

A good path traversal strategy should cover more code with fewer explorations.
It should conform to two core principles.

{\em Few Redundancy Principle.} As is discussed in section~\ref{sec:design:limitation},
DFS/BFS/RS strategies lead to redundant explorations.
The strategy should guarantee that no other basic block can lead to a path through the target basic block.

{\em Few Inconsistent Exploration Principle.}
It is obvious that inconsistent execution may cause exceptions.
The strategy should reduce the number of inconsistent execution as much as it can
to reduce the exception recovery caused by inconsistent execution.

To satisfy the principles,
an intuition is that a basic block becomes a target
if all its ancestors have been targets yet itself is not traced.
The process can be formalized as Equation~\ref{equ:static:intuition}

\begin{equation}
    \label{equ:static:intuition}
    unexplored\left ( n \right )\wedge \forall x
    \bigl(\begin{smallmatrix}
        x \in ancestors\left ( n \right ) \hfill \\
        \wedge n\notin ancestors\left ( x \right )\rightarrow explored\left ( x \right )
    \end{smallmatrix}\bigr)
\end{equation}

{\em unexplored(n)} indicates that {\em n} has not been explored.
{\em explored(x)} indicates that {\em x} has been explored.
{\em x} $\in$ {\em ancestors(n)} indicates that {\em x} is the ancestor of {\em n}
and there is an executable path from {\em x} to {\em n}.
It guarantees a exploration process free of deadlocks.

Obviously, {\em ancestors(x)} cannot be implemented only by dynamic analysis.
Fortunately, the static and dynamic interactive framework helps.

%\vspace{-0.3cm}
\subsection{State Transition}
\label{sec:static:transition}

Before the workflow of {\em Static Path Traversal} is described,
the transition of different states is first demonstrated.

\begin{figure}[b]
    \centering
    \includegraphics[width=0.44\textwidth]{figures/BB-states}
    \caption{BB State Transition.}
    \label{fig:BB-states}
    \vspace{-0.5cm}
\end{figure}

%\vspace{-0.3cm}
\subsubsection{BB State}
This is short for {\em Basic Block State.}
As shown in Figure~\ref{fig:BB-states}, each basic block contains four states:
{\em unexplored}, {\em exploring}, {\em chosen} and {\em done}.

All basic blocks are marked as {\em unexplored} initially,
as well as the ones of dynamically generated code which are unknown for static analysis.
After a trace has been executed, the basic block state is changed to {\em exploring}.

If a basic block is not in the {\em chosen} state,
% TODO: check the meaning
it can be selected as the target basic block if Equation~\ref{equ:static:intuition} is satisfied,
changing the state to {\em chosen}.
% The state of the block is changed to {\em chosen} and the first instruction of it is te target.

If all the indirect control flow transfer targets and exception-based targets in the basic block are explored
and all the function calls are marked as {\em done},
the basic block state is changed to {\em done}.


%\vspace{-0.3cm}
\subsubsection{Path state}
There are two states for each path traced by {\em Dynamic Path Evaluation},
namely {\em exploring} and {\em done}.
A path is marked as {\em exploring} after being traced.
It is changed to {\em done} if all the basic blocks in the path have been {\em explored}
and none of the successors of the basic blocks is {\em unexplored}.

%\vspace{-0.3cm}
\subsubsection{Function state}
Each function has two states: {\em unexplored} and {\em done}.
% to-do
All functions are marked as {\em unexplored} initially.
If all the basic blocks in the CFG of the function are marked as {\em done},
the function is marked as {\em done}.

Note that if a function is recursive,
the call to the functions in the recursive chain will no be taken into account when calculating the states,
so does the loop structure.

\begin{figure}[b]
	\centering
    \includegraphics[width=0.44\textwidth]{figures/StaticWorkflow}
    \caption{The Workflow of Static Path Traverse.}
    \label{fig:StaticWorkflow}
    \vspace{-0.5cm}
\end{figure}

%\vspace{-0.2cm}
\subsection{Workflow}
\label{sec:static:workflow}

The workflow of {\em Static Path Traversal} is shown in Figure~\ref{fig:StaticWorkflow}.
There are mainly three phases, including path iteration, basic block iteration and instruction iteration.
\sysname continually iterates the paths, the basic blocks in the paths and the instructions in the blocks
until the target basic block is found.
The input of each traversal is the path queue maintained by {\em CFG Management}
and the output is the target path.
The details are as follows.

%\vspace{-0.3cm}
\subsubsection{Path Iteration}
There is no {\em exploring} path in the system initially,
so the entry point of the user defined code is selected as the target and wrapped in the output structure.

The path queue maintains all the {\em exploring} paths.
The head path of the queue is picked continually to look for targets.
If a target is found in the path, the basic block iteration begins.
Otherwise, the path is moved to the back of the queue.
When all paths are {\em done} and the queue becomes empty again,
the exploration ends.

%\vspace{-0.3cm}
\subsubsection{BB iteration}
When an {\em exploring} path is picked,
all the basic blocks in the path are iterated.

If a basic block is {\em done},
the successors of it are checked whether they can be the target basic block.
If the successors outside the path cannot be target basic blocks,
the iteration continues.
Otherwise, the target basic block is outputted, as well as the target instruction
which is the first instruction of the basic block.


If a basic block is not {\em done} and it can become the target block, %as equation \ref{equ:static:intuition}
the instruction iteration begins,
or the path iteration continues since no target basic block is picked in the path.


%\vspace{-0.3cm}
\subsubsection{Instruction Iteration}
Every instruction in the given basic block is checked whether the potential targets have been covered.
The targets are given by {\em CFG Management}, which are in fact the ways of leading to different targets.

If all the potential targets of an instruction have been {\em done}, the instruction iteration is continued.
Otherwise, the instruction will be selected as the target instruction,
and the ways guiding the control flow to uncovered targets are outputted together.

%\vspace{-0.3cm}
\subsubsection{Output}
The strategy output, called {\em target path}, contains three parts.
(1) The recorded {\em target basic block} and {\em target instruction}.
% to-do
(2) The {\em pre-path} that can guide the control flow to the target basic block.
(3) The record that can guide the control flow to the target instruction.

Algorithm \ref{alg:workflow} in appendix gives more details.
